<!DOCTYPE html>
<html>
<head>
	<title>HTML2Kotlin - Convert HTML to Kotlin</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">
</head>
<body>
	<div class="editor-wrapper">
		<div class="w-50 html-editor">
			<h1>HTML</h1>
			<textarea id="code" placeholder="Type your HTML code and click convert button" autofocus="autofocus"></textarea>	
		</div>
		<div class="w-50 kotlin-editor">
			<h1>Kotlin</h1>
			<textarea id="result"></textarea>
		</div>
		<div class="clearfix"></div>
		<div class="action">
			<button class="ml-auto" onclick="convert()">Convert (Ctrl + Q)</button>
		</div>
	</div>

	<script type="text/javascript">
		function convert() {
			var html = document.getElementById("code").value;
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("result").value = this.responseText;
				}
			};
			xhttp.open("POST", "convert.php", true);
			xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhttp.send("code=" + html);
		}

		document.onkeyup = function(e) {
			if (e.ctrlKey && e.which == 81) {
				convert();
			}
		};
	</script>
</body>
</html>
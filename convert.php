<?php
require __DIR__ . '/vendor/autoload.php';

use Sunra\PhpSimple\HtmlDomParser;


$htmlCode = $_POST["code"];
$dom = HtmlDomParser::str_get_html($htmlCode);

if (empty($dom)) {
	return;
}

foreach ($dom->root->children as $element) {
	echo "\n" . parse($element);
	echo "\n\ndocument.body!!.appendChild(html)\n";
}

function parse($element, $inner = false, $tab = "", $parentTab = "")
{
	$el = $inner ? "\n\n$tab$element->tag {" : "val html = document.create.$element->tag {";

	if (! empty($element->attr)) {
		foreach ($element->attr as $key => $value) {
			switch (strtolower($key)) {
				case 'class':
					$el .= "\n\t$tab" . "class = setOf(\"$value\")";
					break;

				case 'onclick':
					$el .= "\n\t$tab" . "onClickFunction = {\n$tab\t\t$value \n\t$tab}";
					break;

				case 'style':
					$el .= "\n\t$tab" . "style = \"\"\"\n";
					foreach (explode(";", $value) as $style) {
						if (! empty($style)) { // last item semicolon
							$el .= "$tab\t\t\t\t" . trim($style) . ";\n";
						}
					}
					$el .= "\t\t\t$tab\"\"\"";
					break;
				
				default:
					if (strpos($key, "-"))
						$el .= "\n\t$tab" . "attributes[\"$key\"] = \"$value\"";
					else
						$el .= "\n\t$tab" . "$key = \"$value\"";
					break;
			}
		}
	}

	if (! empty($element->plaintext) && !$element->children()) {
		$el .= "\n\t$tab+ \"" . trim($element->plaintext) . "\"";
	}

	if (! empty($children = $element->children())) {
		$tab = "$tab\t";

		foreach ($children as $child) {
			$el .= parse($child, true, "$tab", $parentTab . "\t");
		}
	}

	$el .= "\n$parentTab}";

	return $el;
}